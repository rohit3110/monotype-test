var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('scss', function() {
    return gulp.src('assets/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('serve', ['scss'], function() {
    
    browserSync.init({
        server: "./"
    });

    gulp.watch("assets/scss/**/*.scss", ['scss']);
    gulp.watch("./*.html").on('change', browserSync.reload);
    //gulp.watch("assets/sass/variable.sass").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);